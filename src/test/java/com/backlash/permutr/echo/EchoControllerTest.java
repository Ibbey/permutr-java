package com.backlash.permutr.echo;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@EnableAutoConfiguration
@SuppressWarnings({"unused"})
public class EchoControllerTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testEchoPost() {

        final Message request = new Message("test request message");
        ResponseEntity<Message> responseEntity =
                restTemplate.postForEntity("/permutr/v1/message", request, Message.class);

        assertNotNull(responseEntity);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertTrue(responseEntity.hasBody());

        final Message body = responseEntity.getBody();

        assertNotNull(body);
        assertEquals("test request message", body.getContent());
    }

}
