package com.backlash.permutr.inventory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class InventoryController {

    @Autowired
    private InventoryService inventoryService;

    private static final String UUID_INVENTORY_PATH = "/permutr/v1/inventory/collection";
    private static final String ADD_INVENTORY_ITEM_PATH = "/permutr/v1/inventory/add";
    private static final String REMOVE_INVENTORY_ITEM_PATH = "/permutr/v1/inventory/remove";

    @RequestMapping(method = RequestMethod.GET, path = UUID_INVENTORY_PATH)
    public final List<InventoryItem> getInventory(@RequestParam(value = "uuid") final String uuid) {

        return inventoryService.getInventory(uuid);

    }

    @RequestMapping(method = RequestMethod.POST, path = ADD_INVENTORY_ITEM_PATH)
    public final boolean addInventoryItem(@RequestBody final InventoryItem item) {

        return inventoryService.addInventoryItem(item);
    }

    @RequestMapping(method = RequestMethod.POST, path = REMOVE_INVENTORY_ITEM_PATH)
    public final boolean removeInventoryItem(@RequestBody final InventoryItem item) {

        return inventoryService.removeInventoryItem(item);
    }

}
