package com.backlash.permutr.inventory;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InventoryService {

    @Autowired
    private InventoryRepository repository;

    @Nullable
    public List<InventoryItem> getEntireInventory() {

        return repository.findAll();
    }

    @Nullable
    public List<InventoryItem> getInventory(final String uuid) {

        if (uuid != null) {
            return repository.findAllByUuid(uuid);
        }

        return null;
    }

    public boolean addInventoryItem(final InventoryItem item) {

        if (item != null) {

            final InventoryItem clone = repository.insert(item);
            if (clone != null && !clone.getId().isEmpty()) {
                return Boolean.TRUE;
            }
        }

        return Boolean.FALSE;
    }

    public boolean removeInventoryItem(final InventoryItem item) {

        if (item != null) {

            repository.delete(item);
            return Boolean.TRUE;
        }

        return Boolean.FALSE;
    }
}
