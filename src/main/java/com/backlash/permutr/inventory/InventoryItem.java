package com.backlash.permutr.inventory;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Locale;

@Document(collection = "inventory")
public class InventoryItem {

    @Id
    private String id;

    private String uuid;

    private String type;

    private String name;

    private String category;

    private String description;

    private Date acquisition;

    private double cost;

    private double estimatedValue;

    private double actualValue;

    private String location;

    private String status;

    private boolean isPublic;

    public InventoryItem() {

    }

    public String getId() {
        return this.id;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(final String value) {
        this.uuid = value;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String value) {
        this.type = value;
    }

    public String getName() {
        return this.name;
    }

    public void setName(final String value) {
        this.name = value;
    }

    public String getCategory() {
        return this.category;
    }

    public void setCategory(final String value) {
        this.category = value;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String value) {
        this.description = value;
    }

    public Date getAcquisition() {
        return this.acquisition;
    }

    public void setAcquisition(final Date value) {
        this.acquisition = value;
    }

    public double getCost() {
        return this.cost;
    }

    public void setCost(final double value) {
        this.cost = value;
    }

    public double getEstimatedValue() {
        return this.estimatedValue;
    }

    public void setEstimatedValue(final double value) {
        this.estimatedValue = value;
    }

    public double getActualValue() {
        return this.actualValue;
    }

    public void setActualValue(final double value) {
        this.actualValue = value;
    }

    public String getLocation() {
        return this.location;
    }

    public void setLocation(final String value) {
        this.location = value;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(final String value) {
        this.status = value;
    }

    public boolean isPublic() {
        return this.isPublic;
    }

    public void setIsPublic(final boolean value) {
        this.isPublic = value;
    }

    @Override
    public String toString() {
        return String.format(
                "InventoryItem[id='%s, uuid='%s', name='%s']",
                this.id, this.uuid, this.name, Locale.US);
    }
}
