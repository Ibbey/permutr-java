package com.backlash.permutr.inventory;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface InventoryRepository extends MongoRepository<InventoryItem, String> {

    List<InventoryItem> findAllByUuid(final String uuid);
}
