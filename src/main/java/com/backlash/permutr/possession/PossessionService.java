package com.backlash.permutr.possession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PossessionService {

    @Autowired
    private PossessionRepository repository;

    public PossessionService() {

    }

    @Nullable
    public List<Possession> getAllPossessions(final String uuid) {

        if (uuid == null) {
            return null;
        }

        return repository.findAllByUuid(uuid);
    }

    @Nullable
    public Possession getPossession(final String uuid, final String name) {

        if (uuid == null || name == null) {
            return null;
        }

        return repository.findByUuidAndName(uuid, name);
    }

    public void addPossession(final Possession possession) {

        if (possession != null) {
            repository.save(possession);
        }
    }

    public void addPossessions(final Iterable<Possession> possessions) {

        if (possessions != null) {
            repository.saveAll(possessions);
        }
    }

    public Double calculatePossessions(final String uuid) {

        if (uuid == null || uuid.length() == 0) {
            return 0.0;
        }

        List<Possession> possessions = this.repository.findAllByUuid(uuid);

        if (possessions != null) {
            Double total = 0.0;
            for (Possession possession : possessions) {
                total += possession.getActualValue();
            }

            return total;
        }

        return 0.0;
    }
}
