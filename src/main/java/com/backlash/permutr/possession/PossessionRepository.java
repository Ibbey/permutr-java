package com.backlash.permutr.possession;

import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface PossessionRepository extends MongoRepository<Possession, String> {

    List<Possession> findAllByUuid(final String uuid);

    Possession findByUuidAndName(final String uuid, final String name);

}
