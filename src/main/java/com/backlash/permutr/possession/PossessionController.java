package com.backlash.permutr.possession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class PossessionController {

    @Autowired
    private PossessionService service;

    private static final String POSSESSIONS_PATH = "/permutr/v1/possessions";
    private static final String FIND_POSSESSION_PATH = "/permutr/v1/possessions/find";
    private static final String ADD_POSSESSION_PATH = "/permutr/v1/possessions/add";
    private static final String CALCULATE_VALUE_PATH = "/permutr/v1/possessions/calculate";

    public PossessionController() {

    }

    @RequestMapping(method = RequestMethod.GET, path = POSSESSIONS_PATH)
    public final List<Possession> getPossesions(@RequestParam(value = "uuid") final String uuid) {

        return service.getAllPossessions(uuid);

    }

    @RequestMapping(method = RequestMethod.GET, path = FIND_POSSESSION_PATH)
    public final Possession findPossession(@RequestParam(value = "uuid") final String uuid,
                                           @RequestParam(value = "name") final String name) {

        return service.getPossession(uuid, name);

    }

    @RequestMapping(method = RequestMethod.POST, path = ADD_POSSESSION_PATH)
    public final void addPossession(@RequestBody final Possession possession) {

        service.addPossession(possession);

    }

    @RequestMapping(method = RequestMethod.GET, path = CALCULATE_VALUE_PATH)
    public final Double calculatePossessions(@RequestParam(value = "uuid") final String uuid) {

        return service.calculatePossessions(uuid);
    }
}
