package com.backlash.permutr.possession;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "possessions")
public class Possession {

    @Id
    private String id;

    private final String uuid;

    private final String name;

    private String description;

    private double perceivedValue;

    private double actualValue;

    private PossessionStatus status;

    public Possession(final String uuid, final String name) {
        this.uuid = uuid;
        this.name = name;
    }

    public String getId() {
        return this.id;
    }

    public String getUuid() {
        return this.uuid;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String value) {
        this.description = value;
    }

    public double getPerceivedValue() {
        return this.perceivedValue;
    }

    public void setPerceivedValue(final double value) {
        this.perceivedValue = value;
    }

    public double getActualValue() {
        return this.actualValue;
    }

    public void setActualValue(final double value) {
        this.actualValue = value;
    }

    public PossessionStatus getStatus() {
        return this.status;
    }

    public void setStatus(final PossessionStatus value) {
        this.status = value;
    }

    @Override
    public String toString() {
        return String.format(
            "Possession[id='%s, uuid='%s', name='%s']",
            this.id, this.uuid, this.name);
    }
}
