package com.backlash.permutr.possession;

public enum PossessionStatus {

    ON_DISPLAY,
    FOR_SALE,
    CONSIGNMENT,
    PENDING_SALE,
    ON_REPAIR,
    IN_PACKAGE,
    IN_TRANSIT
}
