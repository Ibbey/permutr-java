package com.backlash.permutr;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

//CHECKSTYLE:OFF
@SpringBootApplication
@EnableMongoRepositories
@ComponentScan("com.backlash")
//CHECKSTYLE:ON
public class Application implements CommandLineRunner {

    public static final void main(final String[] args) {

        SpringApplication.run(Application.class, args);
    }

    @Override
    public void run(String ...args) {

    }
}
