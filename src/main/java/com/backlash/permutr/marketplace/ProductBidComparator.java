package com.backlash.permutr.marketplace;

import java.util.Comparator;

public class ProductBidComparator implements Comparator<ProductBid> {

    public int compare(ProductBid a, ProductBid b) {
        if (a.getOrder() > b.getOrder()) {
            return -1;
        }

        if (a.getOrder() == b.getOrder()) {
            return 0;
        }

        return 1;
    }
}
