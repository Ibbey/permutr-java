package com.backlash.permutr.marketplace;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.*;

@Document(collection = "product_marketplace")
public class Product {

    @Id
    private String id;

    private final String productId;

    private final String ownerId;

    private final List<ProductBid> bidHistory;

    private String description;

    private double initialPrice;

    private double minimumBid;

    private Date listDate;

    private Date auctionDate;

    private double currentBid;

    private String currentBidderId;

    public Product(final String productId, final String ownerId) {
        this.productId = productId;
        this.ownerId = ownerId;
        this.bidHistory = new ArrayList<>();
    }

    public void startAucton(final double initialPrice,
                            final double minimumBid,
                            final Date auctionDate) {
        startAuction(initialPrice, minimumBid, auctionDate, null);
    }

    public void startAuction(final double initialPrice,
                             final double minimumBid,
                             final Date auctionDate,
                             final String description) {

        this.initialPrice = initialPrice;
        this.minimumBid = minimumBid;
        this.auctionDate = auctionDate;
        this.description = description;
    }

    public List<ProductBid> addBid(final String ownerId, final double amount) {
        final double diff = this.getDifference(amount);
        final Integer max = Collections.max(this.bidHistory, new ProductBidComparator()).getOrder();
        final ProductBid bid = new ProductBid(max + 1, ownerId, amount, diff, new Date());

        this.bidHistory.add(bid);

        return this.bidHistory;
    }

    private double getDifference(final double amount) {

        return amount - this.currentBid;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String value) {
        this.description = value;
    }

    public double getInitialPrice() {
        return this.initialPrice;
    }

    public void setInitialPrice(final double value) {
        this.initialPrice = value;
    }

    public double getMinimumBid() {
        return this.minimumBid;
    }

    public void setMinimumBid(final double value) {
        this.minimumBid = value;
    }

    public Date getListDate() {
        return this.listDate;
    }

    public void setListDate(final Date value) {
        this.listDate = value;
    }

    public Date getAuctionDate() {
        return this.auctionDate;
    }

    public void setAuctionDate(final Date value) {
        this.auctionDate = value;
    }

    public double getCurrentBid() {
        return this.currentBid;
    }

    public void setCurrentBid(final double value) {
        this.currentBid = value;
    }

    public String getCurrentBidderId() {
        return this.currentBidderId;
    }

    public void setCurrentBidderId(final String value) {
        this.currentBidderId = value;
    }
}
