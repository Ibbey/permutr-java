package com.backlash.permutr.marketplace;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface MarketplaceRepository extends MongoRepository<Product, String> {


}
