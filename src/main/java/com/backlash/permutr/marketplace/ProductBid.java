package com.backlash.permutr.marketplace;

import java.util.Date;

public class ProductBid {

    private Integer order;

    private String owner;

    private double amount;

    private double difference;

    private Date timestamp;

    public ProductBid(final Integer order,
                      final String owner,
                      final double amount,
                      final double difference,
                      final Date timestamp) {

        this.order = order;
        this.owner = owner;
        this.amount = amount;
        this.difference = difference;
        this.timestamp = timestamp;
    }

    public Integer getOrder() {
        return this.order;
    }

    public String getOwner() {
        return this.owner;
    }

    public double getAmount() {
        return this.amount;
    }

    public double getDifference() {
        return this.difference;
    }

    public Date getTimestamp() {
        return this.timestamp;
    }
}
