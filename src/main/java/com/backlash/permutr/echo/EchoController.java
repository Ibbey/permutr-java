package com.backlash.permutr.echo;

import org.springframework.web.bind.annotation.*;

import java.util.concurrent.atomic.AtomicLong;

@CrossOrigin("*")
@RestController
@SuppressWarnings({"unused"})
public class EchoController {

    private AtomicLong counter = new AtomicLong();

    @RequestMapping(method = RequestMethod.GET, path = "/permutr/v1/jecho")
    public final Echo echo(@RequestParam(value = "message", defaultValue = "invalid") final String message) {
        return new Echo(counter.incrementAndGet(), message);
    }

    @PostMapping(path = "/permutr/v1/message", produces = "application/json", consumes = "application/json")
    public final Message echo(@RequestBody final Message message) {
        return new Message(message.getContent());
    }
}
