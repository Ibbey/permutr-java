package com.backlash.permutr.echo;

public class Message {

    private String content;

    public String getContent() {
        return content;
    }

    public void setContent(final String content) {
        this.content = content;
    }

    public Message() {

    }

    public Message(final String content) {
        this.content = content;
    }
}
