package com.backlash.permutr.echo;

public class Echo {

    private final long id;
    private final String message;

    public Echo(final long id, final String message) {
        this.id = id;
        this.message = message;
    }

    public final long getId() {
        return this.id;
    }

    public final String getMessage() {
        return this.message;
    }
}
