FROM openjdk:10-jre
LABEL Author="Ryan Barriger"

COPY target/*.jar app.jar

ENTRYPOINT [ "java", "-Djava.security.egd=file:/dev/ ./urandom", "-jar", "/app.jar" ]
